function ShowAccumulate(mainContainer, resultBlock, sendButton) {
    this.mainContainer = document.getElementsByClassName(mainContainer)[0];
    this.resultBlock = document.getElementsByClassName(resultBlock)[0];
    this.sendButton = document.createElement('button');
    this.sendButton.className = sendButton;
    this.sendButton.innerHTML = 'отправить'
    this.index = 0;
    this.dataArr = [];

    this.accummulateData = function() {
        for (var key of this.mainContainer.children[this.index].children) {
            if(key.tagName === 'INPUT' || key.tagName === 'input') {
                this.dataArr.push(key.value);
            }
        }
    };

    this.switch = function(event) {
        if (event.target.tagName === 'BUTTON' && event.target.className !== sendButton || event.target.tagName === 'button' && event.target.className !== sendButton) {
            if(this.index < this.mainContainer.children.length-1) {
                this.mainContainer.children[this.index].classList.remove('active');
                this.accummulateData();
                this.index++;
                this.resultBlock.innerHTML = `<p>Your data is:</p><p>${this.dataArr}</p>`
                this.resultBlock.appendChild(this.sendButton);
                this.mainContainer.children[this.index].classList.add('active');
            }
        }
        if(event.target.className === sendButton) {
            alert('We recieve that, thanks:)');
        }
    }.bind(this);

    this.init = function() {
        this.mainContainer.onclick = this.switch;
    }.bind(this);

    this.init();
}

new ShowAccumulate("forms-container", "total-info", "send-data");