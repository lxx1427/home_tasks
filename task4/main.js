function MoveBlock(blockAdress) {
    this.block = document.getElementsByClassName(blockAdress)[0];

    this.move = function(event) {
        if(event.target.className==='up') {
            this.block.style.top = '-120px';
            this.block.style.left = '0';
        }
        if(event.target.className==='bottom') {
            this.block.style.top = '120px';
            this.block.style.left = '0';
        }
        if(event.target.className==='left') {
            this.block.style.top = '0';
            this.block.style.left = '-120px';
        }
        if(event.target.className==='right') {
            this.block.style.top = '0';
            this.block.style.left = '120px';
        }
        if(event.target.className==='center') {
            this.block.style.top = '0';
            this.block.style.left = '0';
        }
    }.bind(this);
    
    this.init = function() {
        this.block.onclick = this.move;
    }.bind(this);

    this.init();

}

new MoveBlock('block');