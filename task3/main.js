let colors = ['red', 'blue', 'green', 'black', 'brown', 'yellow'];

function SliderColors(data) {
    this.renderContainer = document.getElementsByClassName(data.renderContainer)[0];
    this.buttonPrev = document.getElementsByClassName(data.buttonPrev)[0];
    this.buttonNext = document.getElementsByClassName(data.buttonNext)[0];
    this.buttonRand = document.getElementsByClassName(data.buttonRand)[0];
    this.index = 0;
    this.colorsArr = data.colorsArr;
    this.renderContainer.style.backgroundColor = this.colorsArr[this.index];

    this.nextColor = function() {
        if(this.index<this.colorsArr.length-1) {
            this.index++;
            this.renderContainer.style.backgroundColor = this.colorsArr[this.index];
        } else if (this.index == this.colorsArr.length-1) {
            this.index=0;
            this.renderContainer.style.backgroundColor = this.colorsArr[this.index];
        }
    }.bind(this);

    this.prevColor = function() {
        if(this.index>0) {
            this.index--;
            this.renderContainer.style.backgroundColor = this.colorsArr[this.index];
        } else if (this.index == 0) {
            this.index=this.colorsArr.length-1;
            this.renderContainer.style.backgroundColor = this.colorsArr[this.index];
        }
    }.bind(this);

    this.random = function() {
        this.index = Math.floor(Math.random()*this.colorsArr.length);
        this.renderContainer.style.backgroundColor = this.colorsArr[this.index];
    }.bind(this);

    this.init = function() {
        this.buttonNext.onclick = this.nextColor;
        this.buttonPrev.onclick =  this.prevColor;
        this.buttonRand.onclick = this.random;
    }
    this.init();
};

var colorSlider = new SliderColors({
    renderContainer: 'colors-container', 
    colorsArr: colors, 
    buttonPrev: 'prev', 
    buttonNext: 'next', 
    buttonRand: 'random'
});
