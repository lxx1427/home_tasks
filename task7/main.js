let names = ['terminator', 'rembo', 'dambldor', 'gendalf'];
function UserManager(user) {
    this.data = user.data;
    this.btnFind = document.getElementsByClassName(user.btnFind)[0];
    this.btnList = document.getElementsByClassName(user.btnList)[0];
    this.btnClear = document.getElementsByClassName(user.btnClear)[0];
    this.block = document.getElementsByClassName(user.block)[0];

    this.find = function() {
        this.name = document.getElementsByClassName(user.name)[0].value;
        if(~this.data.indexOf(this.name)) {
            alert('this name already exist in list!');
        } else {
            alert('this name added to list!');
            this.data.push(this.name);
        }
    }.bind(this);

    this.renderList = function() {
        if(this.block.children.length == 0) {
            var index = 1;
            for(var key of this.data) {
                this.block.innerHTML += `<p>${index} : ${key}</p>`
                index++;
            }
        } else {
            var question = confirm('refresh list?');
            if (question) {
                this.block.innerHTML = '';
                var index = 1;
                for(var key of this.data) {
                    this.block.innerHTML += `<p>${index} : ${key}</p>`
                    index++;
                }
            }
        }
    }.bind(this);

    this.clearList = function() {
        this.block.innerHTML ='';
    }.bind(this);


    this.init = function() {
        this.btnFind.onclick = this.find;
        this.btnList.onclick = this.renderList;
        this.btnClear.onclick = this.clearList;
    }.bind(this);

    this.init();

}

new UserManager({
    data: names,
    btnFind: 'button-find',
    btnList: 'button-list',
    btnClear: 'button-clear',
    name: 'input-name',
    block: 'show-text'
})