function RenderName(data) {
    this.renderContainer = document.getElementsByClassName(data.renderContainer)[0];
    this.addButton = document.getElementsByClassName(data.addButton)[0];
    this.removeButton = document.getElementsByClassName(data.removeButton)[0];
    this.ul = document.createElement('ul');
    this.renderContainer.appendChild(this.ul);

    this.checkEmptyContainer = function() {
        if(this.renderContainer.children.length == 0) {
            this.renderContainer.appendChild(this.ul);
        }
    }.bind(this);

    this.renderName = function() {
        this.checkEmptyContainer();
        this.li = document.createElement('li');
        this.value = document.getElementsByClassName(data.inputValue)[0].value;
        this.li.innerHTML = this.value;
        this.ul.appendChild(this.li);
    }.bind(this);

    this.clearContainer = function() {
        this.renderContainer.innerHTML = '';
        this.ul.innerHTML = '';
        console.log(this.renderContainer);
    }.bind(this);

    this.init = function() {
        this.addButton.onclick = this.renderName;
        this.removeButton.onclick = this.clearContainer;
    }.bind(this);
    this.init();
};

new RenderName({
    inputValue: "input-field", 
    addButton: "button-add", 
    removeButton: "button-remove", 
    renderContainer: "selected"
});