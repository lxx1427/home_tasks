let cities = ['Kiev', 'Lviv', 'Odessa', 'Dnepr', 'Kharkiv'];
function RenderList(data) {
    this.containerRender = document.getElementsByClassName(data.containerRender)[0];
    this.containerDeleted = document.getElementsByClassName(data.containerDeleted)[0];
    this.ulRender = document.createElement('ul');
    this.ulRender.className = 'ul-render';
    this.ulDeleted = document.createElement('ul');
    this.ulDeleted.className = 'ul-deleted';
    this.containerRender.appendChild(this.ulRender);
    this.containerDeleted.appendChild(this.ulDeleted);

    this.render = function() {
        for(var key of data.citiesArr) {
        this.li = document.createElement('li');
        this.deleteButtom = document.createElement('button');
        this.deleteButtom.innerHTML = `X`;
        this.li.innerHTML = `${key}`;
        this.li.appendChild(this.deleteButtom);
        this.ulRender.appendChild(this.li);
        }
    }.bind(this);

    this.moveCity = function(event) {
        if(event.target.tagName === 'BUTTON' || event.target.tagName === 'button') {
            if(event.target.parentElement.parentElement.className === 'ul-render') {
                event.target.parentElement.remove();
                this.ulDeleted.appendChild(event.target.parentElement);              
            } else if(event.target.parentElement.parentElement.className === 'ul-deleted') {
                event.target.parentElement.remove();
                this.ulRender.appendChild(event.target.parentElement);              
            };
        }
    }.bind(this);


    this.render();

    this.init = function() {
        document.body.onclick = this.moveCity;
    }.bind(this);

    this.init();
}

new RenderList ({
    containerRender: 'first-block',
    citiesArr: cities,
    containerDeleted: 'second-block',
});