let names = ['vasa', 'pitya', 'anya', 'vera', 'alex'];
function RenderNames(containerForRender, namesArr) {
    this.container = document.getElementsByClassName(containerForRender)[0];
    this.ul = document.createElement('ul');
    this.container.appendChild(this.ul);

    this.render = function() {
        for(var key of namesArr) {
            this.li = document.createElement('li');
            this.li.innerHTML = key;
            this.ul.appendChild(this.li);
            // this.ul.innnerHTML = `<li>${key}</li>`
        }
    }.bind(this);

    this.render();

    this.showName = function(event) {
        if(event.target.tagName === 'LI' || event.target.tagName === 'li') {
            alert(event.target.innerHTML);
        }
    }.bind(this);

    this.ul.onclick = this.showName;
}

new RenderNames('task1', names);